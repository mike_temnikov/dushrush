﻿using UnityEngine;
using System.Collections;
using System.IO;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GameSettings : MonoBehaviour
{

    string text = "Try to beat my highscore in Dash Rush";
    public string subject;
    public string myID;
    string url = "https://play.google.com/store/apps/details?id=com.IndieRoom.DashRush";
    string TopIDstring = ConstGoogle.leaderboard_top;
    public GameObject audioManager;
    public GoogleAnalyticsV3 googleAnalytics;
    static bool gServiceShow = false;

    void Start() {
        if (gServiceShow == false)
        {
            ShowGoogleService();
        }
        gServiceShow = true;
    }

    void ShowGoogleService() {
        GooglePlayGames.PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) => {
            if (success == true)
            {
                googleAnalytics.LogEvent("GoogleService", "login", "login_true", 1);
            }
            else
            {
                googleAnalytics.LogEvent("GoogleService", "login", "login_fail", 0);
            }
        });
    }

    public void SetTop(int score) {
        Social.ReportScore(score, ConstGoogle.leaderboard_top, (bool success) => { });
    }

    public void CheckAchivements(int score) {
        int nGames = PlayerPrefs.GetInt("NumberOfGames");
        switch (nGames) {
            case 5:
                Social.ReportProgress(ConstGoogle.achievement_1, 100.0f, (bool success) => {
                    if (success == true)
                    {
                        PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                        googleAnalytics.LogEvent("Achievement", "Unlocked", "play_5_games", 10);
                    }
                });
                break;
            case 25:
                Social.ReportProgress(ConstGoogle.achievement_2, 100.0f, (bool success) => {
                    if (success == true)
                    {
                        PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                        googleAnalytics.LogEvent("Achievement", "Unlocked", "play_25_games", 10);
                    }
                });
                break;
            case 50:
                Social.ReportProgress(ConstGoogle.achievement_3, 100.0f, (bool success) => {
                    if (success == true)
                    {
                        PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                        googleAnalytics.LogEvent("Achievement", "Unlocked", "play_50_games", 10);
                    }
                });
                break;
            case 100:
                Social.ReportProgress(ConstGoogle.achievement_4, 100.0f, (bool success) => {
                    if (success == true)
                    {
                        PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                        googleAnalytics.LogEvent("Achievement", "Unlocked", "play_100_games", 10);
                    }
                });
                break;
            default:
                Debug.Log("NoAchivements");
                break;
        }
        if (score >= 25) {
            Social.ReportProgress(ConstGoogle.achievement_5, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "pick_25_points", 10);
                }
            });
        }
        if (score >= 50)
        {
            Social.ReportProgress(ConstGoogle.achievement_6, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "pick_50_points", 10);
                }
            });
        }
        if (score >= 100)
        {
            Social.ReportProgress(ConstGoogle.achievement_7, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "pick_100_points", 10);
                }
            });
        }
        if (score >= 150)
        {
            Social.ReportProgress(ConstGoogle.achievement_8, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "pick_150_points", 10);
                }
            });
        }
        int nCollisions = PlayerPrefs.GetInt("WallsCollisions");
        if (nCollisions >= 5) {
            Social.ReportProgress(ConstGoogle.achievement_9, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "5_collisions", 10);
                }
            });
        }
        if (nCollisions >=25) {
            Social.ReportProgress(ConstGoogle.achievement_10, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "25_collisions", 10);
                }
            });
        }
        if (nCollisions >= 50)
        {
            Social.ReportProgress(ConstGoogle.achievement_11, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "50_collisions", 10);
                }
            });
        }
        if (nCollisions >= 100)
        {
            Social.ReportProgress(ConstGoogle.achievement_12, 100.0f, (bool success) => {
                if (success == true)
                {
                    PlayerPrefs.SetInt("NumberAchivements", PlayerPrefs.GetInt("NumberAchivements") + 1);
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "100_collisions", 10);
                }
            });
        }
        if (PlayerPrefs.GetInt("NumberAchivements") == 12) {
            Social.ReportProgress(ConstGoogle.achievement_13, 100.0f, (bool success) => {
                if (success == true)
                {
                    googleAnalytics.LogEvent("Achievement", "Unlocked", "AllAchivement", 10);
                }
            });
        }
    }

    public void ShowTop() {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
        PlayGamesPlatform.Instance.ShowLeaderboardUI(TopIDstring);
        googleAnalytics.LogEvent("ButtonClick", "LeaderBoard", "Clicks", 1);
    }

    public void Rate() {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
        Application.OpenURL("market://details?id=" + myID);
        googleAnalytics.LogEvent("ButtonClick", "Rate", "Clicks", 1);
    }

    public void Share() {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
        StartCoroutine(ShareScreenshot());
        googleAnalytics.LogEvent("ButtonClick", "Share", "Clicks", 1);
    }

    IEnumerator ShareScreenshot() {
        // wait for graphics to render
        yield return new WaitForEndOfFrame();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
        // create the texture
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

        // put buffer into texture
        screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);

        // apply
        screenTexture.Apply();
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO

        byte[] dataToSave = screenTexture.EncodeToPNG();

        string destination = Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");

        File.WriteAllBytes(destination, dataToSave);

        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);

            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text + "\n" + url);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);

            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);
        }
    }
}
