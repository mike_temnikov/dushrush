﻿using UnityEngine;
using UnityEngine.UI;

public class SoundSwitcher : MonoBehaviour {

    public Sprite onSprite, offSprite;
    public static Sprite tempSprite;
    public GameObject glassAudioManager, audioManager, soundButton_1, soundButton_2;

    void Start() {
        if (tempSprite != null) {
            soundButton_1.GetComponent<Image>().sprite = tempSprite;
            soundButton_2.GetComponent<Image>().sprite = tempSprite;
        }
    }

    public void Switch() {
        if (AudioManager.onSound == true)
        {
            soundButton_1.GetComponent<Image>().sprite = offSprite;
            soundButton_2.GetComponent<Image>().sprite = offSprite;
            tempSprite = offSprite;
            AudioManager.onSound = false;
            GlassAudioManager.onSound = false;
            
        }
        else {
            soundButton_1.GetComponent<Image>().sprite = onSprite;
            soundButton_2.GetComponent<Image>().sprite = onSprite;
            tempSprite = onSprite;
            AudioManager.onSound = true;
            GlassAudioManager.onSound = true;
        }
    }
}
