﻿using UnityEngine;
using System.Collections;

public class ColorSwitchWallController : MonoBehaviour {

    public GameObject wall;
    public Color colorPlaneB, colorWallB, colorPlaneT, colorWallT;
    public float timeSwitch;
    float a = 10;

    void Update()
    {
        if (a <= timeSwitch)
        {
            a += Time.deltaTime;
            GetComponent<Renderer>().material.color = Color.Lerp(colorPlaneT, colorPlaneB, a / timeSwitch);
            wall.GetComponent<Renderer>().material.color = Color.Lerp(colorWallT, colorWallB, a / timeSwitch);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Player")
        {
            GetComponent<Renderer>().material.color = colorPlaneT;
            wall.GetComponent<Renderer>().material.color = colorWallT;
            a = 0;
        }
    }
}
