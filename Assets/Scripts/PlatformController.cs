﻿using UnityEngine;

public class PlatformController : MonoBehaviour {

    public float amountPlatform, constrictionSpeed, coeff, speedColorSwitch;
    public Vector3 distance;
    public GameObject plane;
    public Color colorPlatform, colorPlane;
    protected System.Random random;
    protected bool touching;

    void Start()
    {
        random = new System.Random();
    }

    void Update()
    {
        if (touching)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                                                 new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z - 1f),
                                                 Time.deltaTime * constrictionSpeed);
        }

        if (transform.localScale.z <= 0.1)
        {
            gameObject.GetComponent<BoxCollider>().isTrigger = true;
            gameObject.GetComponent<Renderer>().enabled = false;
        }
    }

    void OnCollisionEnter()
    {
        touching = true;
    }

    void OnCollisionExit()
    {
        touching = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Invisible")
        {
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
            gameObject.GetComponent<Renderer>().enabled = true;
            GetComponent<Renderer>().material.color = colorPlatform;
            plane.GetComponent<Renderer>().material.color = colorPlane;
            transform.localScale = new Vector3(transform.localScale.x,
                                                transform.localScale.y,
                                                random.Next(2, 6));
            transform.position = new Vector3(transform.position.x + amountPlatform * distance.x,
                                              transform.position.y,
                                              transform.position.z);
        }
    }
}
