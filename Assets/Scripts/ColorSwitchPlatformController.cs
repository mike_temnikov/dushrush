﻿using UnityEngine;
using System.Collections;

public class ColorSwitchPlatformController : MonoBehaviour {

    public GameObject platform;
    public Color colorPlaneB, colorPlatformB, colorPlaneT, colorPlatformT;
    public float timeSwitch;
    public bool touching;
    private float a = 0;

    void Update()
    {
        if (touching && a <= timeSwitch)
        {
            a += Time.deltaTime;
            GetComponent<Renderer>().material.color = Color.Lerp(colorPlaneT, colorPlaneB, a / timeSwitch);
            platform.GetComponent<Renderer>().material.color = Color.Lerp(colorPlatformT, colorPlatformB, a / timeSwitch);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Player")
        {
            touching = true;
            GetComponent<Renderer>().material.color = colorPlaneT;
            platform.GetComponent<Renderer>().material.color = colorPlatformT;
            a = 0;
        }
    }
}
