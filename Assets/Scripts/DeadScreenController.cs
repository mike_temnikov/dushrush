﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class DeadScreenController : MonoBehaviour {
	
	public GameObject startScreen, scoreGui, topScoreGui, scoreText, audioManager;
    public float topScore;
    public GameObject gameSetting;

	public void View () {
        gameObject.SetActive (true);
        scoreGui.GetComponent<Text>().text = scoreText.GetComponent<Text>().text;
        topScoreGui.GetComponent<Text>().text ="TOP  " + PlayerPrefs.GetFloat("TopScore").ToString();
        gameSetting.GetComponent<GameSettings>().SetTop(Convert.ToInt32(scoreGui.GetComponent<Text>().text));
        gameSetting.GetComponent<GameSettings>().CheckAchivements(Convert.ToInt32(scoreGui.GetComponent<Text>().text));
        if (PlayerPrefs.GetFloat("TopScore") < float.Parse(scoreText.GetComponent<Text>().text)) {
            PlayerPrefs.SetFloat("TopScore", float.Parse(scoreText.GetComponent<Text>().text));
            topScoreGui.GetComponent<Text>().text = "TOP  " + PlayerPrefs.GetFloat("TopScore").ToString();
        }
	}
	
	public void Skip () {
        gameObject.SetActive (false);
    }

	public void Retry () {
        audioManager.GetComponent<AudioManager>().ButtonClickSound();
        Application.LoadLevel ("Main");
        StartScreenController.retry = true;
	}
}
