﻿using UnityEngine;
using System.Collections;

public class PlatformManager : MonoBehaviour {

    public GameObject prefab;
    public Vector3 startPosition, distance;
    public int amountPlatform;
    public System.Random random;

    void Start()
    {
        random = new System.Random();
        GameObject temp = null;
        for (int i = 0; i < amountPlatform; i++)
        {
            temp = prefab;
            temp.transform.position = startPosition;
            temp.transform.localScale = new Vector3(temp.transform.localScale.x,
                                                    temp.transform.localScale.y,
                                                    random.Next(2, 6));
            Instantiate(temp);
            startPosition += distance;
        }
    }
}
