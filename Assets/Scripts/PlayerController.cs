﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public Vector3 jumpImpulse, rebound, posPlatform;
    public float timeSwitch;
    public GameObject fog, deadPlayer, startScreen, deadScreen, gameScreen, scoreText, particles, audioManager, glassAudioManager;
    public bool startGame = false, changeColor;
    public System.Random random;
    public Color[] fogColors = new Color[6];
    private static int nGames = 0, nGame = 1;
    private static int nColor = 0;
    private float score, aTime;
    private Rigidbody rigidBody;
    private bool touchingPlatform = true, collision = false, collisionDamage = false;
    private RigidbodyConstraints previousConstaints;

    void Start()
    {
        if (!AdsManager.showStartInterstitial) {
            startScreen.GetComponent<StartScreenController>().View();
        }
        RenderSettings.fogColor = fogColors[nColor];
        random = new System.Random();
        rigidBody = GetComponent<Rigidbody>();
        previousConstaints = rigidBody.constraints;
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        if (nGames == 3) {
            nGames = 0;
            changeColor = true;
            if (nColor >= 5) {
                nColor = 0;
            }
            else {
                nColor++;
            }
        }
    }

    void Update()
    {
        if (transform.position.y < 0.1)
        {
            StopGame();
        }
        if (changeColor) {
            aTime += Time.deltaTime;
            RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor,fogColors[nColor], aTime / timeSwitch);
        }
        if (System.Math.Abs(transform.position.z) > 0.4) {
            fog.transform.parent = null;
            rigidBody.constraints = previousConstaints;
            startGame = false;
        }
        if (transform.position.x - posPlatform.x > 4)
        {
            GetComponent<BoxCollider>().isTrigger = true;
        }
        if (transform.position.x - posPlatform.x > 5)
        {
            GetComponent<BoxCollider>().isTrigger = false;
        }
        if (startGame)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began /*Input.GetButtonDown("Jump")*/ && touchingPlatform)
            {
                audioManager.GetComponent<AudioManager>().JumpSound();
                rigidBody.AddForce(jumpImpulse, ForceMode.Impulse);
                touchingPlatform = false;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Walls" && !collision)
        {
            audioManager.GetComponent<AudioManager>().WallTouchSound();
            CheckCollision(other);
            PlayerPrefs.SetInt("WallsCollisions", PlayerPrefs.GetInt("WallsCollisions") + 1);
            collision = true;
        }
        if (other.gameObject.name == "DamageWall(Clone)" && !collisionDamage)
        {
            StopGame();
            collisionDamage = true;
        }
        if (other.gameObject.name == "Platform(Clone)" && startGame)
        {
            audioManager.GetComponent<AudioManager>().PlatformTouchSound();
            if ((score + 1) % 10 == 0)
            {
                nGames = 0;
                changeColor = true;
                if (nColor >= 5)
                {
                    nColor = 0;
                }
                else {
                    nColor++;
                }

            } else {
                changeColor = false;
                aTime = 0;
            }

            if (other.transform.position != posPlatform) {
                AddScore(1);
            }
            posPlatform = other.transform.position;
            touchingPlatform = true;
            collision = false;
        }
        if (other.gameObject.name == "Platform")
        {
            touchingPlatform = true;
            collision = false;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "GlassWall(Clone)") {
            glassAudioManager.GetComponent<GlassAudioManager>().GlassSound();
        }
    }

    void CheckCollision(Collision other)
    {
        if (System.Math.Abs(other.transform.position.z - transform.position.z) < transform.localScale.z / 2 + other.transform.localScale.z / 2 - 0.0211f)
        {
            rigidBody.AddForce(rebound, ForceMode.Impulse);
            CollisionWall();
        }
        else
        {
            fog.transform.parent = null;
            rigidBody.constraints = previousConstaints;
            startGame = false;
        }
    }

    void PlayerExplosion()
    {
        nGames++;
        PlayerPrefs.SetInt("NumberOfGames", PlayerPrefs.GetInt("NumberOfGames") + 1);
        deadPlayer.transform.position = transform.position;
        gameObject.SetActive(false);
        audioManager.GetComponent<AudioManager>().DeadSound();
        GameObject temp = Instantiate(deadPlayer);
        foreach (Transform hit in temp.transform)
        {
            hit.gameObject.GetComponent<Rigidbody>().AddForce((hit.position - transform.position) * random.Next(900, 1200), ForceMode.Acceleration);
        }
    }

    void CollisionWall() {
        particles.transform.position = new Vector3(transform.position.x + 0.4f,
                                                   transform.position.y,
                                                   transform.position.z);
        GameObject temp = Instantiate(particles);
        Destroy(temp, 1f);
        foreach (Transform hit in temp.transform) {
            hit.gameObject.GetComponent<Rigidbody>().AddForce((hit.position - temp.transform.position) * random.Next(1000, 2500), ForceMode.Acceleration);
        }
    }

    public void StartGame()
    {
        startGame = true;
        gameScreen.SetActive(true);
    }

    public void StopGame()
    {
        startGame = false;
        fog.transform.parent = null;
        PlayerExplosion();
        ShowAds();
    }

    void ShowAds() {
        nGame++;
        if (nGame >= 5 && PlayerPrefs.GetInt("BuyNoAds") != 1){
            AdsManager.showInterstitial = true;
            nGame = 1;
        }
        else {
            deadScreen.GetComponent<DeadScreenController>().View();
        }
    }

    void AddScore(int n)
    {
        score += n;
        scoreText.GetComponent<Text> ().text = score.ToString();
    }
}
